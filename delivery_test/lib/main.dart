import 'package:delivery_test/internal/application.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const Application());
}
