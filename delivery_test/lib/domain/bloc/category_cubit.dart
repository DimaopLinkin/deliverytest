import 'package:bloc/bloc.dart';

class CategoryCubit extends Cubit<String> {
  CategoryCubit() : super('');

  void chagneCategory(String category) {
    emit(category);
  }
}
