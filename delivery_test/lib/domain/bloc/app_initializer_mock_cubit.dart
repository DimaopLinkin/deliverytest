import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';

class AppInitializerMockCubit extends Cubit<bool> {
  AppInitializerMockCubit() : super(false);

  void loadApplication() async {
    if (!state) {
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      await Future.delayed(const Duration(seconds: 4));
      emit(true);
    }
  }
}
