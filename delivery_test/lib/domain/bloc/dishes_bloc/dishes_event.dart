part of 'dishes_bloc.dart';

@immutable
abstract class DishesEvent {}

class RefreshDishesEvent extends DishesEvent {}

class FilterDishesEvent extends DishesEvent {
  final String category;

  FilterDishesEvent(this.category);
}
