part of 'dishes_bloc.dart';

@immutable
abstract class DishesState {}

class LoadingDishesState extends DishesState {}

class ReadyDishesState extends DishesState {
  final List<Dish> dishes;

  ReadyDishesState(this.dishes);
}

class ErrorDishesState extends DishesState {
  final dynamic error;

  ErrorDishesState(this.error);
}
