import 'package:bloc/bloc.dart';
import 'package:delivery_test/data/repository/dishes_data_repository.dart';
import 'package:delivery_test/domain/model/dish.dart';
import 'package:meta/meta.dart';

part 'dishes_event.dart';
part 'dishes_state.dart';

class DishesBloc extends Bloc<DishesEvent, DishesState> {
  final DishesDataRepository _dishesDataRepository;

  DishesBloc(this._dishesDataRepository) : super(LoadingDishesState()) {
    on<RefreshDishesEvent>((event, emit) async {
      try {
        emit(LoadingDishesState());
        emit(ReadyDishesState(await _dishesDataRepository.getDishes()));
      } catch (e) {
        emit(ErrorDishesState(e));
      }
    });
    on<FilterDishesEvent>((event, emit) async {
      try {
        emit(LoadingDishesState());

        List<Dish> filteredDishes = [];
        List<Dish> dishes = await _dishesDataRepository.getDishes();

        for (var element in dishes) {
          if (element.categories.contains(event.category)) {
            filteredDishes.add(element);
          }
        }
        emit(ReadyDishesState(filteredDishes));
      } catch (e) {
        emit(ErrorDishesState(e));
      }
    });
  }
}
