part of 'restaurant_bloc.dart';

@immutable
abstract class RestaurantEvent {}

class RefreshRestaurantEvent extends RestaurantEvent {}
