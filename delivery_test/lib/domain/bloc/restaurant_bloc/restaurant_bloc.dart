import 'package:bloc/bloc.dart';
import 'package:delivery_test/data/repository/restourant_data_repository.dart';
import 'package:delivery_test/domain/model/restaurant.dart';
import 'package:meta/meta.dart';

part 'restaurant_event.dart';
part 'restaurant_state.dart';

class RestaurantBloc extends Bloc<RestaurantEvent, RestaurantState> {
  final RestaurantDataRepository _restaurantDataRepository;

  RestaurantBloc(this._restaurantDataRepository)
      : super(LoadingRestaurantState()) {
    on<RefreshRestaurantEvent>((event, emit) async {
      try {
        emit(LoadingRestaurantState());
        emit(ReadyRestaurantState(
            await _restaurantDataRepository.getRestaurant()));
      } catch (e) {
        emit(ErrorRestaurantState(e));
      }
    });
  }
}
