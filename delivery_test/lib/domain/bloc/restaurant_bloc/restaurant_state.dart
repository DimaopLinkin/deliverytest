part of 'restaurant_bloc.dart';

@immutable
abstract class RestaurantState {}

class LoadingRestaurantState extends RestaurantState {}

class ReadyRestaurantState extends RestaurantState {
  final Restaurant restaurant;

  ReadyRestaurantState(this.restaurant);
}

class ErrorRestaurantState extends RestaurantState {
  final dynamic error;

  ErrorRestaurantState(this.error);
}
