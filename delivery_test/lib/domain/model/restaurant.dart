class Restaurant {
  final String name;
  final String information;
  final String logo;
  final List<String> categories;

  Restaurant({
    required this.information,
    required this.logo,
    required this.categories,
    required this.name,
  });
}
