class Dish {
  final String name;
  final double price;
  final String photo;
  final int weight;
  final List<String> categories;

  Dish({
    required this.name,
    required this.price,
    required this.photo,
    required this.weight,
    required this.categories,
  });
}
