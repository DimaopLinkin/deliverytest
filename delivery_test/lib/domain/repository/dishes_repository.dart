import 'package:delivery_test/domain/model/dish.dart';

abstract class DishesRepository {
  Future<List<Dish>> getDishes();
}
