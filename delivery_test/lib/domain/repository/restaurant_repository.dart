import 'package:delivery_test/domain/model/restaurant.dart';

abstract class RestaurantRepository {
  Future<Restaurant> getRestaurant();
}
