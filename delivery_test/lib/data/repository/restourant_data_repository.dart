import 'package:delivery_test/data/api/api_util.dart';
import 'package:delivery_test/domain/model/restaurant.dart';
import 'package:delivery_test/domain/repository/restaurant_repository.dart';

class RestaurantDataRepository extends RestaurantRepository {
  final ApiUtil _apiUtil;

  RestaurantDataRepository(this._apiUtil);

  @override
  Future<Restaurant> getRestaurant() {
    return _apiUtil.getRestaurant();
  }
}
