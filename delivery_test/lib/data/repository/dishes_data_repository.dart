import 'package:delivery_test/data/api/api_util.dart';
import 'package:delivery_test/domain/model/dish.dart';
import 'package:delivery_test/domain/repository/dishes_repository.dart';

class DishesDataRepository extends DishesRepository {
  final ApiUtil _apiUtil;

  DishesDataRepository(this._apiUtil);

  @override
  Future<List<Dish>> getDishes() {
    return _apiUtil.getDishes();
  }
}
