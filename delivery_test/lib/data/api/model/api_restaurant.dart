class ApiRestaurant {
  final String name;
  final String information;
  final String logo;
  final List<String> categories;

  ApiRestaurant.fromApi(Map<String, dynamic> map)
      : name = map['name'],
        information = map['information'],
        logo = map['logo'],
        categories =
            (map['categories'] as List).map((item) => item as String).toList();
}
