class ApiDish {
  final String name;
  final double price;
  final String photo;
  final int weight;
  final List<String> categories;

  ApiDish.fromApi(Map<String, dynamic> map)
      : name = map['name'],
        price = map['price'],
        photo = map['photo'],
        weight = map['weight'],
        categories =
            (map['categories'] as List).map((item) => item as String).toList();
}
