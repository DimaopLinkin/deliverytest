import 'package:delivery_test/data/api/model/api_dish.dart';
import 'package:delivery_test/data/api/model/api_restaurant.dart';
import 'package:delivery_test/data/mock_data.dart';

class MockService {
  Future<List<ApiDish>> getDishes() async {
    await Future.delayed(const Duration(seconds: 2));
    return (dishListMock).map((item) => ApiDish.fromApi(item)).toList();
  }

  Future<ApiRestaurant> getRestaurant() async {
    await Future.delayed(const Duration(milliseconds: 500));
    return ApiRestaurant.fromApi(burgerKingRestaurantMock);
  }
}
