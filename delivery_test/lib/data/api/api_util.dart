import 'package:delivery_test/data/api/service/mock_service.dart';
import 'package:delivery_test/data/mapper/dish_mapper.dart';
import 'package:delivery_test/data/mapper/restaurant_mapper.dart';
import 'package:delivery_test/domain/model/dish.dart';
import 'package:delivery_test/domain/model/restaurant.dart';

class ApiUtil {
  final MockService _mockService;

  ApiUtil(this._mockService);

  Future<List<Dish>> getDishes() async {
    return (await _mockService.getDishes())
        .map((apiDish) => DishMapper.fromApi(apiDish))
        .toList();
  }

  Future<Restaurant> getRestaurant() async {
    return RestaurantMapper.fromApi(await _mockService.getRestaurant());
  }
}
