import 'package:delivery_test/data/api/model/api_dish.dart';
import 'package:delivery_test/domain/model/dish.dart';

class DishMapper {
  static Dish fromApi(ApiDish dish) {
    return Dish(
      name: dish.name,
      price: dish.price,
      photo: dish.photo,
      weight: dish.weight,
      categories: dish.categories,
    );
  }
}
