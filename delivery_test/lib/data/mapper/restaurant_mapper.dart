import 'package:delivery_test/data/api/model/api_restaurant.dart';
import 'package:delivery_test/domain/model/restaurant.dart';

class RestaurantMapper {
  static Restaurant fromApi(ApiRestaurant restaurant) {
    return Restaurant(
        name: restaurant.name,
        information: restaurant.information,
        logo: restaurant.logo,
        categories: restaurant.categories);
  }
}
