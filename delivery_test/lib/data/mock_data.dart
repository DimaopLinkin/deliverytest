var burgerKingRestaurantMock = {
  'name': 'Бургер Кинг',
  'information':
      '''Ежедневно в рестораны БУРГЕР КИНГ во всем мире приходит более одиннадцати миллионов посетителей. И неудивительно: ведь наши рестораны известны вкусом фирменных блюд и отличным соотношением цены и качества.

Основанный в 1954 году, БУРГЕР КИНГ занимает второе по величине место в мире среди сетей ресторанов быстрого питания, специализирующихся на гамбургерах. Оригинальный бургер «Воппер», ставка на ингредиенты высшего качества, фирменные рецепты и комфорт для посещения всей семьей — вот то, что уже пятьдесят с лишним лет успешной работы является отличительной чертой нашего бренда.

Мы готовы работать со всеми, кто готов в долгосрочной перспективе выполнять стандарты доставки: Доставка 30 минут, «Трехточечная» (10 минут на прибытие в ресторан, 20 минут доставка до адресата, 20 минут возврат в ресторан оборудования), мед книжки у курьеров.

Вы нам интересны в любом случае: если вы можете доставлять в нескольких городах или только в одном городе. Готовы рассматривать долгосрочные контракты с таксопарками.''',
  'logo':
      'https://en.wikipedia.org/wiki/Burger_King#/media/File:Burger_King_2020.svg',
  'categories': [
    'Новинки',
    'Бургеры из говядины',
    'Бургеры из курицы и рыбы',
    'Креветки'
  ],
};

var angusShefXLMock = {
  'name': 'АНГУС Шеф XL',
  'price': 689.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/ec9e9ff026325424bb14cf3395be1846.webp',
  'weight': 454,
  'categories': ['Новинки', 'Бургеры из говядины'],
};

var angusShefMock = {
  'name': 'АНГУС Шеф',
  'price': 449.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/69bf436f2eae634db1703e94b189a211.webp',
  'weight': 338,
  'categories': ['Новинки', 'Бургеры из говядины'],
};

var grandCheeseMock = {
  'name': 'Гранд Чиз',
  'price': 199.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/456213a9cdaa811dbec353a1b79f4ad9.webp',
  'weight': 178,
  'categories': ['Новинки', 'Бургеры из говядины'],
};

var grandCheeseFreshMock = {
  'name': 'Гранд Чиз Фреш',
  'price': 239.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/a082a58c01ae8b98b3351289d9b3529b.webp',
  'weight': 205,
  'categories': ['Новинки', 'Бургеры из говядины'],
};

var grandCheeseFreshXLMock = {
  'name': 'Гранд Чиз Фреш XL',
  'price': 339.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/e4aef630aa37e1e024c50cbd219f64f7.webp',
  'weight': 278,
  'categories': ['Новинки', 'Бургеры из говядины'],
};

var doubleVopperCheeseMock = {
  'name': 'Двойной Воппер с сыром',
  'price': 409.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/7baabffde14bc525758c09bfdb4c7ecc.webp',
  'weight': 368,
  'categories': ['Бургеры из говядины'],
};

var fishKingMock = {
  'name': 'Фиш Кинг',
  'price': 199.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/e28050ed762b146b0650bf2640cc73e0.webp',
  'weight': 182,
  'categories': ['Бургеры из курицы и рыбы'],
};

var chickenburgerMock = {
  'name': 'Чикенбургер',
  'price': 74.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/343d3e9776cc44831cef1160f3c1f884.webp',
  'weight': 134,
  'categories': ['Бургеры из курицы и рыбы'],
};

var cesarKingMock = {
  'name': 'Цезарь Кинг',
  'price': 129.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/2113b60dbb545d393bc86ced8808b974.webp',
  'weight': 153,
  'categories': ['Бургеры из курицы и рыбы'],
};

var doubleFishKingMock = {
  'name': 'Двойной Фиш Кинг',
  'price': 239.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/2256b1f7babd7a24b34e99fe1582e167.webp',
  'weight': 268,
  'categories': ['Бургеры из курицы и рыбы'],
};

var rodeoChickenMock = {
  'name': 'Родео Чикен Гамбургер',
  'price': 99.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/6ce3942fd38eb384c3168e202dbb84d8.webp',
  'weight': 135,
  'categories': ['Новинки', 'Бургеры из курицы и рыбы'],
};

var saladCesarMock = {
  'name': 'Салат Цезарь с креветками',
  'price': 369.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/1c3d682fe26ac262924fa57da395548b.webp',
  'weight': 185,
  'categories': ['Креветки'],
};

var kingKrevetkiMock = {
  'name': 'Кинг креветки 5 штук',
  'price': 309.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/871a5155673e83c4bd96c4bba7de5c11.webp',
  'weight': 80,
  'categories': ['Креветки'],
};

var shrimpRollMock = {
  'name': 'Шримп Ролл',
  'price': 259.99,
  'photo':
      'https://orderapp-static.burgerkingrus.ru/x512/mobile_image/73a139fc31c43f1cb32df447487675e3.webp',
  'weight': 166,
  'categories': ['Креветки'],
};

List<Map<String, dynamic>> get dishListMock => [
      angusShefXLMock,
      angusShefMock,
      grandCheeseMock,
      grandCheeseFreshMock,
      grandCheeseFreshXLMock,
      doubleVopperCheeseMock,
      fishKingMock,
      chickenburgerMock,
      cesarKingMock,
      doubleFishKingMock,
      rodeoChickenMock,
      saladCesarMock,
      kingKrevetkiMock,
      shrimpRollMock,
    ];
