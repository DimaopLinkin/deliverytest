import 'package:delivery_test/domain/bloc/restaurant_bloc/restaurant_bloc.dart';
import 'package:delivery_test/internal/dependencies/application_component.dart';
import 'package:delivery_test/presentation/design/application_design.dart';
import 'package:delivery_test/presentation/dish_list/dish_list_body/dish_list_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DishListScreen extends StatefulWidget {
  const DishListScreen({Key? key}) : super(key: key);

  @override
  _DishListScreenState createState() => _DishListScreenState();
}

class _DishListScreenState extends State<DishListScreen> {
  final RestaurantBloc _restaurantBloc = DishListModule.restaurantBloc();

  // TODO: И здесь вопрос
  @override
  void initState() {
    super.initState();
    _restaurantBloc.add(RefreshRestaurantEvent());
  }

  @override
  void dispose() {
    _restaurantBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RestaurantBloc, RestaurantState>(
      bloc: _restaurantBloc,
      listener: (context, state) {
        if (state is ErrorRestaurantState) {
          _showErrorDialog(state.error);
        }
      },
      child: BlocBuilder<RestaurantBloc, RestaurantState>(
        bloc: _restaurantBloc,
        builder: (context, state) {
          if (state is ReadyRestaurantState) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: kBgColor,
                leading: const Icon(Icons.keyboard_backspace),
                title: Text(
                  state.restaurant.name,
                ),
                centerTitle: true,
                actions: const [
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Icon(Icons.info_outline_rounded),
                  )
                ],
              ),
              body: DishListBody(categories: state.restaurant.categories),
            );
          } else {
            return const LoaderPage();
          }
        },
      ),
    );
  }

  dynamic _showErrorDialog(dynamic error) {
    return ErrorDialogWidget.showErrorDialog(
      context,
      error: error,
      handlerButton: () => _restaurantBloc.add(RefreshRestaurantEvent()),
    );
  }
}
