import 'package:delivery_test/domain/bloc/category_cubit.dart';
import 'package:delivery_test/domain/bloc/dishes_bloc/dishes_bloc.dart';
import 'package:delivery_test/internal/dependencies/dish_list_module.dart';
import 'package:delivery_test/presentation/design/application_design.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'dish_card.dart';

class DishListBody extends StatefulWidget {
  final List<String> categories;

  const DishListBody({Key? key, required this.categories}) : super(key: key);

  @override
  State<DishListBody> createState() => _DishListBodyState();
}

class _DishListBodyState extends State<DishListBody> {
  final CategoryCubit _categoryCubit = DishListModule.categoryCubit();
  final DishesBloc _dishesBloc = DishListModule.dishesBloc();

  @override
  void initState() {
    super.initState();
    _categoryCubit.chagneCategory(widget.categories.first);
    _dishesBloc.add(FilterDishesEvent(_categoryCubit.state));
  }

  @override
  void dispose() {
    _dishesBloc.close();
    _categoryCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = (MediaQuery.of(context).size.width) / 2 - 21;
    return BlocBuilder<CategoryCubit, String>(
      bloc: _categoryCubit,
      builder: (context, state) {
        return Column(
          children: [
            _categoriesList(),
            _dishesGrid(width),
          ],
        );
      },
    );
  }

  Expanded _dishesGrid(double width) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 10.0),
        child: BlocListener<DishesBloc, DishesState>(
          bloc: _dishesBloc,
          listener: (context, state) {
            if (state is ErrorDishesState) {
              _showErrorDialog(state.error);
            }
          },
          child: BlocBuilder<DishesBloc, DishesState>(
            bloc: _dishesBloc,
            builder: (context, state) {
              if (state is ReadyDishesState) {
                return GridView(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: width / (width * 1.5),
                    crossAxisCount:
                        MediaQuery.of(context).size.width > 450 ? 3 : 2,
                    mainAxisSpacing: 10.0,
                    crossAxisSpacing: 10.0,
                  ),
                  children: [
                    ...state.dishes.map((dish) => DishCard(dish: dish)),
                  ],
                );
              } else {
                return const LoaderPage();
              }
            },
          ),
        ),
      ),
    );
  }

  SizedBox _categoriesList() {
    return SizedBox(
      height: 46,
      child: ListView(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
        scrollDirection: Axis.horizontal,
        children: [
          ...widget.categories.map((category) => _category(category)),
        ],
      ),
    );
  }

  Widget _category(category) {
    return GestureDetector(
      onTap: () {
        _categoryCubit.chagneCategory(category);
        _dishesBloc.add(FilterDishesEvent(category));
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(0, 16 / 4, 0, 0),
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: category == _categoryCubit.state
                ? kActiveCategoryColor
                : Colors.transparent,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
            child: SizedBox(
              height: 40.0,
              child: Center(
                child: Text(
                  category,
                  style: TextStyle(
                    color: category == _categoryCubit.state
                        ? kActiveCategoryTextColor
                        : kInactiveCategoryTextColor,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  dynamic _showErrorDialog(dynamic error) {
    return ErrorDialogWidget.showErrorDialog(
      context,
      error: error,
      handlerButton: () => _dishesBloc.add(RefreshDishesEvent()),
    );
  }
}
