import 'package:delivery_test/domain/model/dish.dart';
import 'package:delivery_test/presentation/design/application_design.dart';
import 'package:flutter/material.dart';

class DishCard extends StatelessWidget {
  final Dish dish;
  const DishCard({
    Key? key,
    required this.dish,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var dishCardWidth = screenWidth / 2 - 21;
    return ClipRRect(
      borderRadius: BorderRadius.circular(25.0),
      child: SizedBox(
        width: dishCardWidth,
        height: dishCardWidth * 1.5,
        child: Column(
          children: [
            Container(
              color: kDishBgColor,
              child: SizedBox(
                child: Image.network(dish.photo),
                height: dishCardWidth,
                width: dishCardWidth,
              ),
            ),
            Container(
              height: dishCardWidth * 0.5,
              width: dishCardWidth,
              color: kDishInfoBgColor,
              child: Padding(
                padding: const EdgeInsets.all(14.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      dish.name,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          dish.price.toString() + ' ₽',
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        Text(
                          dish.weight.toString(),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
