import 'package:delivery_test/presentation/design/application_design.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class SplashBody extends StatelessWidget {
  const SplashBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: kSplashBgColor,
      child: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.width,
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: [
              const RiveAnimation.asset('assets/animations/burger.riv'),
              Positioned(
                top: 100,
                child: Text(
                  'BURGER KING',
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
