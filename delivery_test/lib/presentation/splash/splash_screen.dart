import 'package:delivery_test/domain/bloc/app_initializer_mock_cubit.dart';
import 'package:delivery_test/presentation/design/application_design.dart';
import 'package:delivery_test/presentation/dish_list/dish_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'splash_body.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: applicationTheme,
      title: 'Бургер Кинг',
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AppInitializerMockCubit, bool>(
        builder: (context, state) {
          if (!state) {
            return const SplashBody();
          } else {
            return const DishListScreen();
          }
        },
      ),
    );
  }
}
