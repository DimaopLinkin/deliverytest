import 'package:flutter/material.dart';

import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

// TODO: Прошу прощения, за копипаст, на правах - "ого, какая классная
// штука а не позаимствовать ли её :). Т.к. ошибок при загрузке мок
// данных ТЕОРЕТИЧЕСКИ быть не должно, возможно, он и не выскочит ни разу
//
// update: Все таки выскочил из-за String моковых данных вместо double,
// но на тесты всего и вся я вкладывать время не стал :)
class ErrorDialogWidget {
  static showErrorDialog(
    BuildContext context, {
    dynamic error,
    title = 'Error',
    String labelButton = 'Repeat',
    Function? handlerButton,
  }) {
    showPlatformDialog(
      context: context,
      builder: (_) => PlatformAlertDialog(
        title: const Text('Error'),
        content: Text(error == null
            ? 'An error has occurred, write to the developer about this'
            : error.toString()),
        actions: <Widget>[
          PlatformDialogAction(
            child: PlatformText(labelButton),
            onPressed: () {
              Navigator.pop(context);
              if (handlerButton != null) {
                handlerButton();
              }
            },
          ),
        ],
      ),
    );
  }
}
