import 'package:delivery_test/presentation/design/application_design.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class LoaderPage extends StatelessWidget {
  const LoaderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        decoration: BoxDecoration(
          color: kSplashBgColor,
          borderRadius: BorderRadius.circular(100.0),
        ),
        height: 200.0,
        width: 200.0,
        child: const RiveAnimation.asset('assets/animations/burger.riv'),
      ),
    );
  }
}
