import 'package:flutter/material.dart';

const kActiveCategoryColor = Color(0xFFE4C840);

const kBgColor = Color(0xFF000000);
const kDishBgColor = Color(0xFFFFFFFF);
const kDishInfoBgColor = Color(0xFF2E2E2E);
const kSplashBgColor = Color(0xFF2E2E2E);

const kLogoColor = Color(0xFFCF2200);

const kActiveCategoryTextColor = Color(0xFF000000);
const kInactiveCategoryTextColor = Color(0xFF525252);

const kMainTextDishColor = Color(0xFFFFFFFF);
const kShadowedTextDishColor = Color(0xFFAAAAAA);
