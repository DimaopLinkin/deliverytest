import 'package:flutter/material.dart';

import 'application_design.dart';

final applicationTheme = ThemeData(
  backgroundColor: kSplashBgColor,
  scaffoldBackgroundColor: kBgColor,
  textTheme: const TextTheme(
    bodyText2: TextStyle(color: kMainTextDishColor),
    bodyText1: TextStyle(
      color: kShadowedTextDishColor,
      fontWeight: FontWeight.w100,
      fontSize: 12.0,
    ),
    headline4: TextStyle(color: kLogoColor, fontWeight: FontWeight.bold),
  ),
);
