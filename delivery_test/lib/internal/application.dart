import 'package:delivery_test/domain/bloc/app_initializer_mock_cubit.dart';
import 'package:delivery_test/presentation/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Application extends StatelessWidget {
  const Application({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AppInitializerMockCubit()..loadApplication(),
      child: const SplashScreen(),
    );
  }
}
