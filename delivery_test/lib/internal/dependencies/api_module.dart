import 'package:delivery_test/data/api/api_util.dart';
import 'package:delivery_test/data/api/service/mock_service.dart';

class ApiModule {
  static ApiUtil? _apiUtil;

  static ApiUtil apiUtil() {
    _apiUtil ??= ApiUtil(MockService());
    return _apiUtil!;
  }
}
