import 'package:delivery_test/data/repository/dishes_data_repository.dart';
import 'package:delivery_test/internal/dependencies/api_module.dart';

class DishesRepositoryModule {
  static final dishesRepository = DishesDataRepository(ApiModule.apiUtil());
}
