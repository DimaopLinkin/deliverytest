import 'package:delivery_test/data/repository/restourant_data_repository.dart';
import 'package:delivery_test/internal/dependencies/api_module.dart';

class RestaurantRepositoryModule {
  static final restaurantRepositoryModule =
      RestaurantDataRepository(ApiModule.apiUtil());
}
