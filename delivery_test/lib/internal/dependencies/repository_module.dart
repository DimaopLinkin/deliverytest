import 'package:delivery_test/data/repository/dishes_data_repository.dart';
import 'package:delivery_test/data/repository/restourant_data_repository.dart';
import 'package:delivery_test/internal/dependencies/api_module.dart';

class RepositoryModule {
  static final dishesRepository = DishesDataRepository(ApiModule.apiUtil());
  static final restaurantRepository =
      RestaurantDataRepository(ApiModule.apiUtil());
}
