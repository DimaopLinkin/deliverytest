import 'package:delivery_test/domain/bloc/category_cubit.dart';
import 'package:delivery_test/domain/bloc/dishes_bloc/dishes_bloc.dart';
import 'package:delivery_test/domain/bloc/restaurant_bloc/restaurant_bloc.dart';
import 'package:delivery_test/internal/dependencies/dishes_repository_module.dart';
import 'package:delivery_test/internal/dependencies/restaurant_repository_module.dart';

// TODO: Задать вопрос, почему мы здесь делаем через статичные
// функции, а не через static final поля
class DishListModule {
  static RestaurantBloc restaurantBloc() {
    return RestaurantBloc(
        RestaurantRepositoryModule.restaurantRepositoryModule);
  }

  static CategoryCubit categoryCubit() {
    return CategoryCubit();
  }

  static DishesBloc dishesBloc() {
    return DishesBloc(DishesRepositoryModule.dishesRepository);
  }
}
